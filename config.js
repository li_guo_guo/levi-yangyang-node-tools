// 以下参数根据自己的需要进行修改：
module.exports = {
  // 使用抓包工具抓包羊了个羊的接口数据
  // 获取到羊了个羊游戏请求的header中t值,必须修改为自己的
  header_t:
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTQzMjA3NDMsIm5iZiI6MTY2MzIxODU0MywiaWF0IjoxNjYzMjE2NzQzLCJqdGkiOiJDTTpjYXRfbWF0Y2g6bHQxMjM0NTYiLCJvcGVuX2lkIjoiIiwidWlkIjoyMzQ4NDYxMiwiZGVidWciOiIiLCJsYW5nIjoiIn0.yS4t-TcCroH06xtmBagpuAD3aXFJNCyZP2lk2ZUKQso",
  // 获取到的header中的user-agent值  这个值可以j正常情况不用修改
  header_user_agent:
    "Mozilla/5.0 (Linux; Android 6.0.1; MuMu Build/V417IR; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/86.0.4240.99 XWEB/3262 MMWEBSDK/20220204 Mobile Safari/537.36 MMWEBID/1266 MicroMessenger/8.0.20.2100(0x28001438) Process/appbrand0 WeChat/arm32 Weixin Android Tablet NetType/WIFI Language/zh_CN ABI/arm64 MiniProgramEnv/android",
  // 每次请求间隔时间 单位 秒
  time_second: 5,
  // 每次请求的次数  这个值越大就请求得越快 尽量不要太大
  tiems_count: 10,
  // 设定的完成耗时，单位s，默认-1随机表示随机生成1min~1h之内的随机数，设置为正数则为固定
  cost_time: -1,
  // 需要通关的次数，默认1  实际通关次数会大于这个值
  cycle_count: 10000,
}
