# levi-yangyang-node-tools  使用成功了别忘了点个start
羊了个羊助手，当前版本：`V2022.0.1Beta`，

### 介绍

本项目旨在为你快速通关羊了个羊，上手需要一定动手能力以及门槛，持续更新中...

所需工具推荐任选：：Fiddler【PC】、HttpCarry【Android】、Stream【iphone】

代码见 `app.js` ，配置文件 `config.js` 需要自行按需修改，具体怎么使用见下方使用教程，t 的值使用软件怎么获取这里不描述，自行探索，懂的都懂，感谢 issues 贡献方法老铁们，集思广益汇集力量，本内容会随时间发生改变，请自行分辨。

效果图： 建议完成时间控制在几分钟到几十分钟不等。

![效果图](./preview1.png)


### 抓取 羊了个羊 请求的header中t值，下载、解压查看教程即可
```shell
https://wwz.lanzoub.com/iQNuI0br962b
密码:jjjj
```

### 使用教程
1、 克隆本仓库地址或者下载文件到本地，安装好 node 环境

[node环境最新稳定版安装包，下载、解压安装即可](https://wwz.lanzoub.com/ix05V0bthtli)

```shell
git clone https://gitee.com/li_guo_guo/levi-yangyang-node-tools.git
```
2、使用工具抓取羊了个羊小游戏的包，获取到header中的t值

3、打开 `config.js` ，修改抓取的必填t值，其他参数按照注释按需修改
```shell
  // 获取到羊了个羊游戏请求的header中t值,必须修改为自己的
  header_t: "*******.**************",
  // 获取到的header中的user-agent值  这个值可以j正常情况不用修改
  header_user_agent:
    "Mozilla/5.0 (Linux; Android 6.0.1; MuMu Build/V417IR; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/86.0.4240.99 XWEB/3262 MMWEBSDK/20220204 Mobile Safari/537.36 MMWEBID/1266 MicroMessenger/8.0.20.2100(0x28001438) Process/appbrand0 WeChat/arm32 Weixin Android Tablet NetType/WIFI Language/zh_CN ABI/arm64 MiniProgramEnv/android",
  // 每次请求间隔时间 单位 秒
  time_second: 5,
  // 每次请求的次数  这个值越大就请求得越快 尽量不要太大
  tiems_count: 10,
  // 设定的完成耗时，单位s，默认-1随机表示随机生成1min~1h之内的随机数，设置为正数则为固定
  cost_time: -1,
  // 需要通关的次数，默认1  实际通关次数会大于这个值
  cycle_count: 10000,
```
4、运行run.bat

### 免责声明

1. 本仓库发布的 `levi-yangyang-node-tools` (下文均用本项目代替) 项目中涉及的任何脚本，仅用于测试和学习研究，禁止用于商业用途，不能保证其合法性，准确性，完整性和有效性，请根据情况自行判断。

2. 本项目内所有资源文件，禁止任何公众号、自媒体进行任何形式的转载、发布，禁止直接改项目名二次发布。

3. 作者对任何脚本问题概不负责，包括但不限于由任何脚本错误导致的任何损失或损害.

4. 请勿将本项目的任何内容用于商业或非法目的，否则后果自负。

5. 如果任何单位或个人认为该项目的脚本可能涉嫌侵犯其权利，则应及时通知并提供身份证明，所有权证明，我们将在收到认证文件后删除相关脚本。

6. 以任何方式查看此项目的人或直接或间接使用本项目的任何脚本的使用者都应仔细阅读此声明。作者保留随时更改或补充此免责声明的权利。一旦使用并复制了任何相关脚本或本项目，则视为您已接受此免责声明。

7. 您必须在下载后的24个小时之内，从您的电脑或手机中彻底删除上述内容。

8. 本项目遵循GPL-3.0 License协议，如果本特别声明与GPL-3.0 License协议有冲突之处，以本特别声明为准。

9. 任何擅自改变计算机信息网络数据属于违法行为，本项目不提供成品可运行程序，仅做学习研究使用。

`您使用或者复制了本仓库且本人制作的任何代码或项目，则视为已接受此声明，请仔细阅读。`
